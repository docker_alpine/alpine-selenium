# alpine-selenium
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-selenium)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-selenium)



----------------------------------------
### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-selenium/x64)
### aarch64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-selenium/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : [Selenium](https://www.selenium.dev/)
    - Selenium is a suite of tools for automating web browsers.



----------------------------------------
#### Run

```sh
docker run -d \
		   -p 4444:4444/tcp \
		   forumi0721/alpine-selenium:[ARCH_TAG]
```



----------------------------------------
#### Usage

* URL : [http://localhost:4444/](http://localhost:4444/)



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 4444/tcp           | Access port                                      |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /conf.d            | Config data                                      |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

