#!/bin/sh

export DISPLAY=:99.0
export DBUS_SESSION_BUS_ADDRESS="/dev/null"

cd /app/selenium
#java -cp /app/selenium/*:. org.openqa.grid.selenium.GridLauncherV3
#java -jar /app/selenium/selenium-server-standalone.jar -config /app/selenium/config.json -role standalone
exec java -jar /app/selenium/selenium-server-standalone.jar -role standalone -port 4444

