#!/bin/sh

export DISPLAY=:99.0

exec /usr/bin/Xvfb ${DISPLAY} -screen 0 1280x1024x16 -dpi 96 -ac +extension RANDR

